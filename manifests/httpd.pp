class linuxbase::httpd {

    package { 'httpd':
        ensure => installed
    }

    package { 'php':
        ensure => installed
    }

    package { 'php-mysql':
        ensure => installed
    }

    file {'/etc/httpd/conf.modules.d/10-php.conf':
        ensure => 'present',
        source => 'puppet:///modules/linuxbase/httpd/10-php.conf'
    }

    package { 'libcurl':
        ensure => installed
    }

    package { 'libcurl-devel':
        ensure => installed
    }

}