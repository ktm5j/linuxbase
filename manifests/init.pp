class linuxbase {
  include linuxbase::packages

  file {"/${partition}":
    ensure => 'present',
    seltype => 'home_root_t'
  }
  
  file { '/root/.emacs.d':
    ensure => 'directory'
  }

  file {'/root/.emacs.d/puppet-mode.el':
    ensure => 'present',
    source => 'puppet:///modules/linuxbase/emacs.d/puppet-mode.el'
  }

  file {'/root/.emacs':
    ensure => 'present',
    source => 'puppet:///modules/linuxbase/emacs.d/emacs'
  }
}
