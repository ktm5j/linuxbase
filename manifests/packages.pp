class linuxbase::packages {
  package { 'emacs-nox':
    ensure => present
  }

  package { 'emacs-php-mode':
      ensure => installed
  }

  package { 'vim':
    ensure => present
  }

  package { 'ipa-client':
    ensure => present
  }

  package { 'lbzip2':
    ensure => present
  }

  package { 'lbzip2-utils':
    ensure => present
  }

  package { 'bzip2':
    ensure => present
  }

  package { 'libxml2':
    ensure => present
  }

  package { 'libxml2-devel':
    ensure => present
  }

  package { 'openssl-devel':
    ensure => present
  }

  package { 'openssl-libs':
    ensure => present
  }

  package { 'libpqxx-devel':
    ensure => present
  }

  package { 'libpqxx':
    ensure => present
  }

  package { 'tcl':
    ensure => present
  }

  package { 'tcl-devel':
    ensure => present
  }

  package { 'glibc-static':
    ensure => present
  }

}
