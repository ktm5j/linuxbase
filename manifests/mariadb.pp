class linuxbase::mariadb {

    package { 'mariadb':
        ensure => installed
    }

    package { 'mariadb-server':
        ensure => installed
    }

    package { 'mariadb-libs':
        ensure => installed
    }

}